package SirSlender.vericode;

import SirSlender.vericode.dbfactory.Table;

public class tables {
	public static final Table CODE_TABLE = new Table("mcverify_users", "id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
															+ "UUID VARCHAR(160),"
															+ "code INT");
}
