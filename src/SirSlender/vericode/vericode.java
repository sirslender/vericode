package SirSlender.vericode;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import SirSlender.vericode.dbfactory.Database;
import SirSlender.vericode.dbfactory.DatabaseConfigBuilder;
import SirSlender.vericode.dbfactory.DatabaseFactory;

public class vericode extends JavaPlugin {
	Logger log = Bukkit.getLogger();
	
	@Override
	public void onEnable() {
		try {
			getServer().getPluginManager().registerEvents(new randnum(this), this);
			DatabaseFactory factory = new DatabaseFactory(this);
			factory.generateConfigSection();
			ConfigurationSection section = getConfig().getConfigurationSection("database");
			File sqliteFile = new File(getDataFolder(), "database.db");
			DatabaseConfigBuilder config = new DatabaseConfigBuilder(section, sqliteFile);
			Database database = factory.getDatabase(config);
			database.connect(); //Its time for the real work
			db = database;
			db.registerTable(tables.CODE_TABLE);
			storage = config.getStorage();
			db.close();
			log.info("[VeriCode enabled!]");
		} catch (Exception e) {
			e.printStackTrace();
			this.setEnabled(false);
			log.info("[VeriCode could not be enabled!]");
		}
		projectName = getConfig().getString("Title");
	}
	
	@Override
	public void onDisable() {
		log.info("[VeriCode disabled!]");
	}
	
	public Database db;
	public String storage;
	public static String projectName;
}
