package SirSlender.vericode;

import java.sql.SQLException;
import java.util.Random;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import SirSlender.vericode.dbfactory.Database;

public class randnum implements Listener{
	private vericode plugin;
	Database db = plugin.db;
	String projectName = vericode.projectName;
	
	public randnum(vericode plugin){
		this.plugin = plugin;
	}
	
	public String generate() {
		//Define the length of the word (in your case - number), you can set at to any number you want
		int length = 8;
		 
		//This String contains all the numbers (you can also add letters to it, to generate a random mix of letters and numbers)
		String numbers = "123456789";
		 
		//define the result
		String myresult = "";
		 
		//random generator
		Random rand = new Random();
		 
		//use while loop to check if the length of the current number equlas to the lehngth defined above
		while (myresult.length() < length){
		 
		//generate a random number, the number will be used to get one of the numbers from the String numbers later on
		int x = rand.nextInt(numbers.length());
		 
		//add one of the numbers to the current result
		myresult = myresult + numbers.charAt(x);
		}
		return myresult;
	}
	
	public boolean codeExists(String code) throws SQLException {
		db.connect();
		if (db.contains(tables.CODE_TABLE, "code", code)) {
			return true;
		} else {
			return false;
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onLogin(PlayerLoginEvent e) throws SQLException{
		db.connect();
		Player p = e.getPlayer();
		UUID id = p.getUniqueId();
		String code;
		if (db.contains(tables.CODE_TABLE, "UUID", id)){
			code = (String) db.get(tables.CODE_TABLE, "UUID", "code", id);
			e.disallow(e.getResult(), "Your " + projectName + " verification ID is " + code);
		} else {
			code = generate();
			while (codeExists(code)){
				code = generate();
			}
			db.set(tables.CODE_TABLE, null, id, code);
			e.disallow(e.getResult(), "Your " + projectName + " verification ID is " + code);
		}
		db.close();
	}
}
